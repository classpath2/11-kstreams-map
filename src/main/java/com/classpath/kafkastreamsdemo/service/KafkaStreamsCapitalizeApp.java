package com.classpath.kafkastreamsdemo.service;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Properties;

public class KafkaStreamsCapitalizeApp {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaStreamsCapitalizeApp.class);

    public static void main(String[] args) throws Exception {


        MockDataProducer.produceRandomTextData();

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "app_id");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "165.22.219.150:9092");


        Serde<String> stringSerde = Serdes.String();

        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> simpleFirstStream = builder.stream("src-topic", Consumed.with(stringSerde, stringSerde));


        KStream<String, String> upperCasedStream = simpleFirstStream.mapValues((value) -> value.toUpperCase());

        upperCasedStream.to( "out-topic", Produced.with(stringSerde, stringSerde));

        upperCasedStream.print(Printed.<String, String>toSysOut().withLabel("Capitalized App"));


        KafkaStreams kafkaStreams = new KafkaStreams(builder.build(),props);
        LOG.info("Started");
        kafkaStreams.start();
        Thread.sleep(35000);
        LOG.info("Shutting down the App now");
        kafkaStreams.close();
        MockDataProducer.shutdown();

    }
}