package com.classpath.kafkastreamsdemo.service;

import com.github.javafaker.*;
import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

    public static final int NUMBER_TEXT_STATEMENTS = 15;

    private static Faker dateFaker = new Faker();

    private DataGenerator() {
    }

    public static List<String> generateRandomText() {
        List<String> phrases = new ArrayList<>(NUMBER_TEXT_STATEMENTS);
        Faker faker = new Faker();

        for (int i = 0; i < NUMBER_TEXT_STATEMENTS; i++) {
            ChuckNorris chuckNorris = faker.chuckNorris();
            phrases.add(chuckNorris.fact());
        }
        return phrases;
    }

}
